import React from 'react'
// react dom
import { Link } from 'react-router-dom'
// mui
import { Button } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import MaterialTable from 'material-table'
import { useQuery } from '@apollo/react-hooks'

import { GET_BOOKS } from '../../graphql/queries/book'

import { Loading } from '../../components'

export default () => {

  const {error, loading, data} = useQuery(GET_BOOKS)

  if (error) return <h1> error </h1>
  if (loading) return <Loading />

  const displayTitle = () => <h3> Books List </h3>


  return (
    <>
      <Link to={`/books/add`}>
        <Button
          color="primary"
          variant="contained"
          style={{marginBottom: 15}}
        >
          Add Book 
        </Button>
      </Link>
      <MaterialTable
        // className={classes.root}
        columns={[
          {
            title: 'edit', field: 'url', width: 10,
            render: ({ _id }) => <Link to={`/books/edit/${_id}`}><MoreVertIcon /></Link>
          },
          { title: 'Name', field: 'name' },
          { title: 'Genre', field: 'genre' },
          { title: 'Author', field: 'author.name'}
        ]}
        data={data.getBooks}
        options={{
          pageSize: 1000,
          pageSizeOptions: [20,50,100,1000],
          paginationType: 'stepped',
          emptyRowsWhenPaging: false,
          // searchFieldStyle: {
          //   direction: 'ltr'
          // },
        }}
        title={displayTitle()}
      />
    </>
  )
}