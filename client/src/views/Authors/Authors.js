import React from 'react'
// react dom
import { Link } from 'react-router-dom'
// mui
import { Button } from '@material-ui/core'

import { useQuery } from '@apollo/react-hooks'
import { GET_AUTHORS } from '../../graphql/queries/authors'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import MaterialTable from 'material-table'
import { Loading } from '../../components'



export default () => {

  const { error, loading, data } = useQuery(GET_AUTHORS)


  if (error) return <h1> error </h1>
  if (loading) return <Loading />

  
  const displayTitle = () => <h3> Authors List </h3>

  return (
    <>
      <Link to={`/authors/add`}>
        <Button
          color="primary"
          variant="contained"
          style={{marginBottom: 15}}
        >
          Add Author 
        </Button>
      </Link>

      <MaterialTable
        // className={classes.root}
        columns={[
          {
            title: 'edit', field: 'url', width: 10,
            render: ({ _id }) => <Link to={`/authors/edit/${_id}`}><MoreVertIcon /></Link>
          },
          { title: 'Name', field: 'name' },
        ]}
        data={data.getAuthors}
        options={{
          pageSize: 1000,
          pageSizeOptions: [20,50,100,1000],
          paginationType: 'stepped',
          emptyRowsWhenPaging: false,
          // searchFieldStyle: {
          //   direction: 'ltr'
          // },
        }}
        title={displayTitle()}
      />

    </>
  )
}