import React, {useState} from 'react'

import {useQuery, useMutation} from '@apollo/react-hooks'
import { GET_AUTHORS } from '../../graphql/queries/authors'
import { ADD_BOOK } from '../../graphql/mutations/book'

import MuiAlert from '@material-ui/lab/Alert'

import {
  Divider,
  Checkbox,
  FormControlLabel,
  Button,
  Grid,
  Card,
  CardHeader,
  CardContent,
  TextareaAutosize,
  Snackbar,
  TextField
} from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'

import services from '../../services'

import { Loading } from '../../components'

const { MoveME } = services

const Alert = props => <MuiAlert elevation={6} variant="filled" {...props} />

export default () => {

  const [addBook] = useMutation(ADD_BOOK)


  // form states
  const [name, setName] = useState('')
  const [genre, setGenre] = useState('')
  const [authorid, setAuthorId] = useState('choose')

  // Snack States
  const [emptyFieldError, setemptyFieldError] = useState(false)

  /// Snack Functions
  const handleFieldErrorOpen = () => {
    setemptyFieldError(true)
  };

  const handleFieldErrorClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setemptyFieldError(false)
  };

  // End of Snack Functions

  const { error, loading, data } = useQuery(GET_AUTHORS)
  
  if (error) return <h1> error </h1>
  if (loading) return <Loading />

  const onSubmitBook = async () => {
    try {
      const { data: { addBook: { msg } } } = await addBook({
        variables: {
          name,
          genre,
          authorid
        }
      })

      if (msg === 'successfully added this Book') MoveME('books')

      
    } catch (error) {
      alert('alert')
    }
  }

  return (
    <>
            
      <Snackbar open={emptyFieldError} autoHideDuration={2500} onClose={handleFieldErrorClose}>
        <Alert onClose={handleFieldErrorClose} severity="error">
          please fill the specified fields   
        </Alert>
      </Snackbar>

      <Card>
        <CardHeader
          title="Add Book"
        />
        <Divider />
        <CardContent>
          <ValidatorForm
            onSubmit={() => onSubmitBook()}
            onError={errors => handleFieldErrorOpen()}
          >
            <Grid
              container
              spacing={1}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextValidator
                  fullWidth
                  label="name"
                  margin="dense"
                  onChange={({target: {value}}) => setName(value)}
                  value={name}
                  variant="outlined"
                  validators={['required']}
                  errorMessages={['Please Enter a Name']}
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextValidator
                  fullWidth
                  label="genre"
                  margin="dense"
                  onChange={({target: {value}}) => setGenre(value)}
                  value={genre}
                  variant="outlined"
                  validators={['required']}
                  errorMessages={['Please Enter a genre']}
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                  <TextField
                    fullWidth
                    label="author name"
                    margin="dense"
                    onChange={({ target: {value}}) => setAuthorId(value) }
                    required
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={authorid}
                    variant="outlined"
                  >
                    <option value='choose'> choose </option>
                    {data.getAuthors.map(({name, _id}) => (
                      <option
                        key={_id}
                        value={_id}
                      >
                        {name}
                      </option>
                    ))}
                  </TextField>
              </Grid>
            </Grid>
            <Button
              color="primary"
              variant="contained"
              style={{margin: 24}}
              type="submit"
              // disabled={inProgress}
            >
              submit
            </Button>
          </ValidatorForm>
        </CardContent>

      </Card>
    </>
  )
}