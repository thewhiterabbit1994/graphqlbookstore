import React, { useState } from 'react'

import MuiAlert from '@material-ui/lab/Alert'

import {
  Divider,
  Checkbox,
  FormControlLabel,
  Button,
  Grid,
  Card,
  CardHeader,
  CardContent,
  TextareaAutosize,
  Snackbar,
} from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'

import { useMutation } from '@apollo/react-hooks'

import { ADD_AUTHOR } from '../../graphql/mutations/authors'

import services from '../../services'


const { MoveME } = services

const Alert = props => <MuiAlert elevation={6} variant="filled" {...props} />



export default () => {

  const [addAuthor] = useMutation(ADD_AUTHOR)


  // form states
  const [name, setName] = useState('')

  // Snack States
  const [emptyFieldError, setemptyFieldError] = useState(false)

    /// Snack Functions
    const handleFieldErrorOpen = () => {
      setemptyFieldError(true)
    };
  
    const handleFieldErrorClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
  
      setemptyFieldError(false)
    };
  
    // End of Snack Functions
  
  const onSubmitAuthor = async () => {
    try {
      const { data: { addAuthor: { msg } } } = await addAuthor({
        variables: {
          name
        }
      })

      if (msg === 'successfully added this author') MoveME('authors')

      
    } catch (error) {
      alert('alert')
    }
  }

  return (
    <>
      
      <Snackbar open={emptyFieldError} autoHideDuration={2500} onClose={handleFieldErrorClose}>
        <Alert onClose={handleFieldErrorClose} severity="error">
          please fill the specified fields   
        </Alert>
      </Snackbar>

      <Card>
        <CardHeader
          title="Add Author"
        />
        <Divider />
        <CardContent>
          <ValidatorForm
            onSubmit={() => onSubmitAuthor()}
            onError={errors => handleFieldErrorOpen()}
          >
            <Grid
              container
              spacing={1}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextValidator
                  fullWidth
                  label="name"
                  margin="dense"
                  onChange={({target: {value}}) => setName(value)}
                  value={name}
                  variant="outlined"
                  validators={['required']}
                  errorMessages={['Please Enter a Name']}
                />
              </Grid>
            </Grid>
            <Button
              color="primary"
              variant="contained"
              style={{margin: 24}}
              type="submit"
              // disabled={inProgress}
            >
              submit
            </Button>
          </ValidatorForm>
        </CardContent>

      </Card>

    </>
  )


}