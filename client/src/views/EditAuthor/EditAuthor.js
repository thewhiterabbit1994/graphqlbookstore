import React, { useState } from 'react'

import MuiAlert from '@material-ui/lab/Alert'

import {
  Divider,
  Checkbox,
  FormControlLabel,
  Button,
  Grid,
  Card,
  CardHeader,
  CardContent,
  TextareaAutosize,
  Snackbar,
} from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'

import services from '../../services'

 
import {useQuery, useMutation} from '@apollo/react-hooks'
import { GET_AUTHOR } from '../../graphql/queries/authors'
import { EDIT_AUTHOR } from '../../graphql/mutations/authors'

import { Loading } from '../../components'

const Alert = props => <MuiAlert elevation={6} variant="filled" {...props} />
const { MoveME } = services


export default ({ match: { params: { _id } } }) => {

  const [editAuthor] = useMutation(EDIT_AUTHOR)


  // form states
  const [name, setName] = useState('')

  // Snack States
  const [emptyFieldError, setemptyFieldError] = useState(false)

  /// Snack Functions
  const handleFieldErrorOpen = () => {
    setemptyFieldError(true)
  };

  const handleFieldErrorClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setemptyFieldError(false)
  };

  // End of Snack Functions

  const onSubmitEdit = async () => {
    try {
      const { data: { editAuthor: { msg } } } = await editAuthor({
        variables: {
          name,
          _id
        }
      })

      if (msg === 'successfully edited this author') MoveME('authors')

      
    } catch (error) {
      alert('alert')
    }  }

  const { error, loading, data } = useQuery(GET_AUTHOR, {
    variables: {
      _id
    },
    onCompleted: () => {
      setName(data.getAuthor.name)
    }
  })
  
  if (error) return <h1> error </h1>
  if (loading) return <Loading />


  console.log(data)

  return (
    <>
            
      <Snackbar open={emptyFieldError} autoHideDuration={2500} onClose={handleFieldErrorClose}>
        <Alert onClose={handleFieldErrorClose} severity="error">
          please fill the specified fields   
        </Alert>
      </Snackbar>

      <Card>
        <CardHeader
          title="Edit Author"
        />
        <Divider />
        <CardContent>
          <ValidatorForm
            onSubmit={() => onSubmitEdit()}
            onError={errors => handleFieldErrorOpen()}
          >
            <Grid
              container
              spacing={1}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextValidator
                  fullWidth
                  label="name"
                  margin="dense"
                  onChange={({target: {value}}) => setName(value)}
                  value={name}
                  variant="outlined"
                  validators={['required']}
                  errorMessages={['Please Enter a Name']}
                />
              </Grid>
            </Grid>
            <Button
              color="primary"
              variant="contained"
              style={{margin: 24}}
              type="submit"
              // disabled={inProgress}
            >
              submit
            </Button>
          </ValidatorForm>
        </CardContent>

      </Card>
    </>
  )
}
