import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

import {
  Dashboard,
  Books,
  Authors,
  AddBook,
  AddAuthor,
  EditAuthor
} from './views';

const Routes = () => {
  return (
    <Switch>
      
      {/* validation */}

      <Redirect
        exact
        from="/dashboard"
        to='/dashboard/home'
      />

      <RouteWithLayout
        component={Dashboard}
        exact
        layout={MainLayout}
        path="/"
      />

      
      {/* Book Section */}

      <RouteWithLayout
        component={Books}
        exact
        layout={MainLayout}
        path="/books"
      />

      <RouteWithLayout
        component={AddBook}
        exact
        layout={MainLayout}
        path="/books/add"
      />



      {/* Author Section  */}
            
      <RouteWithLayout
        component={Authors}
        exact
        layout={MainLayout}
        path="/authors"
      />

      <RouteWithLayout
        component={AddAuthor}
        exact
        layout={MainLayout}
        path="/authors/add"
      />

      <RouteWithLayout
        component={EditAuthor}
        exact
        layout={MainLayout}
        path="/authors/edit/:_id"
      />

    </Switch>
  );
};

export default Routes;
