import gql from 'graphql-tag'

export const ADD_AUTHOR = gql`
  mutation ADD_AUTHOR($name: String!) {
    addAuthor(name: $name) {
      msg
    }
  }
`

export const EDIT_AUTHOR = gql`
  mutation EDIT_AUTHOR($name: String!, $_id: ID!) {
    editAuthor(name: $name, _id: $_id) {
      msg
    }
  }
`