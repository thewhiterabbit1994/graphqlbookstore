import gql from 'graphql-tag'

export const ADD_BOOK = gql`
  mutation ADD_BOOK($name: String!, $genre: String!, $authorid: ID!) {
    addBook(name: $name, genre: $genre, authorid: $authorid) {
      msg
    }
  }
`