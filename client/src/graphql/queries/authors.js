import gql from 'graphql-tag'

export const GET_AUTHORS = gql`
  query {
    getAuthors {
      _id
      name
    }
  }
`

export const GET_AUTHOR = gql` 
  query GET_AUTHOR($_id: ID!) {
    getAuthor(_id: $_id) {
      _id
      name
    }
  }
`