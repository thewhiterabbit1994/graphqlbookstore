import gql from 'graphql-tag'

export const GET_BOOKS = gql`
  query {
    getBooks {
      _id
      name
      genre
      author {
        _id
        name
      }
    }
  }
`