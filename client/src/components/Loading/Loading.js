import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles((theme) => ({
  Loading: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 99999
  }
}));

export default () => {
  const classes = useStyles();

  return <LinearProgress className={classes.Loading} />

}