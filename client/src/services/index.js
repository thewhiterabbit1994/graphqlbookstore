export default {
  getRelativeTime: date => {
    const seconds = Math.floor((Date.now() - Date.parse(date)) / 1000)
    const array = date.split('T')[0].split('-')
    
    let interval = Math.floor(seconds / 31536000);
    if (interval > 1) {
      if (array[1] === "01") return `January ${array[2]}, ${array[0]}`
      if (array[1] === "02") return `February ${array[2]}, ${array[0]}`
      if (array[1] === "03") return `March ${array[2]}, ${array[0]}`
      if (array[1] === "04") return `April ${array[2]}, ${array[0]}`
      if (array[1] === "05") return `May ${array[2]}, ${array[0]}`
      if (array[1] === "06") return `June ${array[2]}, ${array[0]}`
      if (array[1] === "07") return `July ${array[2]}, ${array[0]}`
      if (array[1] === "08") return `August ${array[2]}, ${array[0]}`
      if (array[1] === "09") return `September ${array[2]} , ${array[0]}`
      if (array[1] === "10") return `October ${array[2]}, ${array[0]}`
      if (array[1] === "11") return `November ${array[2]}, ${array[0]}`
      if (array[1] === "12") return `December ${array[2]}, ${array[0]}`
    }

    interval = Math.floor(seconds / 2592000);
    if (interval >= 1) {
      return `${interval} ماه پیش`;
    }

    interval = Math.floor(seconds / 86400);
    if (interval >= 1) {
      return `${interval} روز پیش`;
    }

    interval = Math.floor(seconds / 3600);
    if (interval >= 1) {
      return `${interval} ساعت پیش`;
    }

    interval = Math.floor(seconds / 60);
    if (interval >= 1) {
      return `${interval} دقیقه پیش`;
    }

    return `${Math.floor(seconds)} ثانیه پیش`;
  },
  MoveME: str => window.location.assign(`${window.location.origin}/${str}`),
  getExactTime: date => {
    const thisDate = new Date(date)
    const turnThisIntoMonth = n => {
      if (n === 1) return 'January'
      if (n === 2) return 'February'
      if (n === 3) return 'March'
      if (n === 4) return 'April'
      if (n === 5) return 'May'
      if (n === 6) return 'June'
      if (n === 7) return 'July'
      if (n === 8) return 'August'
      if (n === 9) return 'September'
      if (n === 10) return 'October'
      if (n === 11) return 'November'
      if (n === 12) return 'December'
    }
    return `${thisDate.getFullYear()} / ${turnThisIntoMonth(thisDate.getMonth() + 1)} / ${thisDate.getDate()}`
  },
  getFactorStatus: status => {
    if (status === 'paymentConfirmation') return 'در حال پردازش'
    if (status === 'factorApproved') return 'تایید فاکتور'
    if (status === 'Accountant') return 'پروسه صدور فاکتور رسمی'
    if (status === 'Stock') return 'پردازش در انبار'
    if (status === 'Logistic') return 'ارسال'
  },
  numberWithCommas: num => {

    let x = Math.ceil(Number(num))

    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");
    return x;
  }
}