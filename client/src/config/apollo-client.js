import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { onError } from 'apollo-link-error'
import { setContext } from 'apollo-link-context'
import { createUploadLink } from 'apollo-upload-client'
import Cookies from 'universal-cookie';

import constants from './constants'
import services from '../services'

const { MoveME } = services

const cookies = new Cookies();

const { domain, EndPoint } = constants

const cache = new InMemoryCache();
// 
const errorLink = onError(({ graphQLErrors, networkError, operation }) => {

  if (graphQLErrors) {

    graphQLErrors.forEach(({ message, location, path }) => {

      console.log(`message: ${message}, location: ${location}, path:${path}`)

      if (message === 'Unathorized') {
        return window.location.assign(domain)
      }
    })
  }

  if (networkError) {
    console.log(networkError)
    
  }
})

const authLink = setContext(async (_, { headers, ...rest }) => {

  if (false) {
    
    return {
      ...rest,
    } 
  } else {
    return null
  }
})

const httpLink = createUploadLink({ uri: `${domain}${EndPoint}` })

const link = ApolloLink.from([errorLink, authLink, httpLink])

const client = new ApolloClient({
  cache,
  link
})

export default client;