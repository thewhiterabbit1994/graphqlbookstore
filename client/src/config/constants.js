export default {
  domain: window.location.origin.search('4299') !== -1 ? "http://localhost:4000" : `${window.location.origin}:4000` ,
  frontPort: 4299,
  EndPoint: '/graphql',
}