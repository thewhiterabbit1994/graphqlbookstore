import React from 'react'
import ReactDOM from 'react-dom'
import * as serviceWorker from './serviceWorker'
import App from './App'
import { ApolloProvider } from '@apollo/react-hooks'
import client from './config/apollo-client'

const MyComponent = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
)

ReactDOM.render(<MyComponent />, document.getElementById('root'));

serviceWorker.unregister();
