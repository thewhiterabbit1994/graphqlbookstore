import mongoose, { Schema } from 'mongoose'

const AuthorSchema = new Schema({
	name: String,
}, {
	timestamps: true
});

export default mongoose.model('Author', AuthorSchema);