import mongoose, { Schema } from 'mongoose'

const BookSchema = new Schema({
  name: String,
  genre: String,
  authorid: String
}, {
	timestamps: true
});

export default mongoose.model('Book', BookSchema);