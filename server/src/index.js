import express from 'express'
import { createServer } from 'http'
import { ApolloServer } from 'apollo-server-express'
import { readFileSync } from "fs"
import path from "path"
import compression from 'compression'

import resolvers from './graphql/resolvers'

import './config/db'

const port = 4000

const app = express()

// ignore 
app.use(compression()) //gzip
app.use(express.static(__dirname + '/public'));

const typeDefs = readFileSync(path.join(__dirname, "/graphql/schema.graphql"), { encoding: "utf8" })

const server = new ApolloServer({
  typeDefs,
  resolvers,
  // context: ({ req }) => ({
  //   user: req.user,
  //   ip: req.ip,
  //   ua: req.headers['user-agent']
  // }),
  // playground: false
})

server.applyMiddleware({ app, path: '/graphql' })

const httpServer = createServer(app)

httpServer.listen(port, () => console.log(`app is running on port ${port}`))