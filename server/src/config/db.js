


/* eslint-disable no-console */
import mongoose from 'mongoose';

import constants from './constants';
const { DB_URL, DB_SETTINGS } = constants

// TO BE HONEST I DON'T KNOW WHAT THIS DOES
mongoose.Promise = global.Promise;

// mongoose.set('debug', true); // debug mode on

try {
  mongoose.connect(DB_URL, DB_SETTINGS);
} catch (err) {
  mongoose.createConnection(DB_URL, DB_SETTINGS);
}

mongoose.connection
  .once('open', () => console.log('connected to mongodb'))
  .on('error', e => {
    console.log('error connecting to mongodb')
    throw e;
  });
