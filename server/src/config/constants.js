export default {
  port: 4000,
  DB_URL: 'mongodb://localhost/graphqlBookStore',
  DB_SETTINGS: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  },
}