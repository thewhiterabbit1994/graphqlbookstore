
import Author from '../../models/Author'

export default {
  getAuthors: async () => Author.find({}),
  getAuthor: async (_, { _id }) => Author.findById(_id),
  addAuthor: async (_, data) => {

    try {
      await Author.create(data)

      return {
        msg: 'successfully added this author'
      }

    } catch (error) {
      throw error
    }

  },
  editAuthor: async (_, { _id, ...rest }) => {
    try {
      
      const thisAuthor = await Author.findById(_id)

      if (!thisAuthor || !thisAuthor._id) throw new Error('no such author found')
      
      await Author.findByIdAndUpdate(_id, { $set: { ...rest } })

      return {
        msg: 'successfully edited this author'
      }
    
    } catch (error) {
      throw error
    }
  }
}