
import Book from '../../models/Book'

export default {
  getBooks: async () => Book.find({}),
  getBook: async (_, { _id }) => Book.findById(_id),
  addBook: async (_, data) => {

    try {
      await Book.create(data)

      return {
        msg: 'successfully added this Book'
      }

    } catch (error) {
      throw error
    }

  },
  editBook: async (_, { _id, ...rest }) => {
    try {
      
      const thisBook = await Book.findById(_id)

      if (!thisBook || !thisBook._id) throw new Error('no such Book found')
      
      await Book.findByIdAndUpdate(_id, { $set: { ...rest } })

      return {
        msg: 'successfully edited this Book'
      }
    
    } catch (error) {
      throw error
    }
  },
  getBookMutation: async (_, { _id }) => Book.findById(_id),
}