
import authorResolver from './author-resolver'
import bookResolver from './book-resolver'

import Author from '../../models/Author'
import Book from '../../models/Book'

const {
  getAuthors,
  getAuthor,
  addAuthor,
  editAuthor
} = authorResolver

const {
  getBooks,
  getBook,
  addBook,
  editBook,
  getBookMutation
} = bookResolver


export default {

  Book: {
    author: async ({ authorid }) => Author.findById(authorid)
  },

  Author: {
    books: async ({ _id }) => Book.find({ authorid: _id })
  },

  Query: {
    getAuthors,
    getAuthor,
    getBooks,
    getBook,
  },
  Mutation: {
    addAuthor,
    editAuthor,
    addBook,
    editBook,
    getBookMutation
  }
}